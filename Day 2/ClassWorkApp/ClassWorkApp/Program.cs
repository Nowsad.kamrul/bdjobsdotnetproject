﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWorkApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your Number please");
            float number = Convert.ToSingle(Console.ReadLine());
            

            if (number >=96)
                {
                    Console.WriteLine("Your grade is A+");

                }
                else if (number >= 91)
                {
                    Console.WriteLine("Your grade is A");

                }
                else if (number >= 90)
                {
                    Console.WriteLine("Your grade is A-");

                }
                else if (number >= 85)
                {
                    Console.WriteLine("Your grade is B+");

                }
                else if (number >= 80)
                {
                    Console.WriteLine("Your grade is B");

                }
                else if (number >= 75)
                {
                    Console.WriteLine("Your grade is B-");

                }
                else if (number >= 70)
                {
                    Console.WriteLine("Your grade is C+");

                }
                else if (number >= 65)
                {
                    Console.WriteLine("Your grade is C");

                }
                else if (number >= 60)
                {
                    Console.WriteLine("Your grade is C-");

                }
                else if (number >= 55)
                {
                    Console.WriteLine("Your grade is D+");

                }
                else if (number >= 50)
                {
                    Console.WriteLine("Your grade is D");

                }
                else if (number >= 45)
                {
                    Console.WriteLine("You are fail");

                }
            

            else
            {
                Console.WriteLine("Your input is invalid");
            }
            Console.Read();
        }
    }
}
