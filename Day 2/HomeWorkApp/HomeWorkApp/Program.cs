﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkApp
{
    class Program
    {
        private static void Main(string[] args)
        {
            int inputNumber;
            Console.WriteLine("Enter a Number");

            inputNumber = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("The reverse series is:");

            int counter = ((inputNumber * 2) - 1);
            int anotherNumber = inputNumber;
            for (int i = 1; i <= counter; i++)
            {
                if (i > inputNumber)
                {
                    anotherNumber = anotherNumber - 1;
                    Console.Write(anotherNumber + " ");
                }

                else
                {
                    Console.Write(i+" ");
                }
            }

            Console.ReadKey();

        }
    }


}


