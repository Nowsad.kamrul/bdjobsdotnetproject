﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter Your first number  :");


            int firstNumber = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter Your second number  :");
            int secondNumber = Convert.ToInt32(Console.ReadLine());
            int result = sum(firstNumber, secondNumber);
            int result1 = sub(firstNumber, secondNumber);
            int result2 = div(firstNumber, secondNumber);
            int result3 = mul(firstNumber, secondNumber);
            Console.WriteLine("sum is " + result);
            Console.WriteLine("sub is " + result1);
            Console.WriteLine("div is " + result2);
            Console.WriteLine("mul is " + result3);
            Console.ReadKey();


        }

        private static int sum(int fNumber, int sNumber)
        {
            int sum = fNumber + sNumber;
            return sum;
        }
        private static int sub(int fNumber, int sNumber)
        {
            int sub = fNumber - sNumber;
            return sub;
        }
        private static int mul(int fNumber, int sNumber)
        {
            int mul = fNumber * sNumber;
            return mul;
        }
        private static int div(int fNumber, int sNumber)
        {
            int div = fNumber / sNumber;
            return div;
        }
    }
}
