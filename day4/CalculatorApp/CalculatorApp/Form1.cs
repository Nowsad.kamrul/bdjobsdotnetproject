﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorApp
{
    public partial class CalculatorAppUI : Form
    {
         int fNumber;
         int sNumber;

        public CalculatorAppUI()
        {
            InitializeComponent();
        }

        private void CalculatorAppUI_Load(object sender, EventArgs e)
        {

        }

        private void firstNumber_Click(object sender, EventArgs e)
        {

        }

        private void FirstNumberTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void SecondNumberTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void sumButton_Click(object sender, EventArgs e)

        {
            fNumber = Convert.ToInt32(FirstNumberTextBox.Text);
            sNumber = Convert.ToInt32(SecondNumberTextBox.Text);

            loadLabel.Text = (fNumber + sNumber).ToString();

        }

        private void subButton_Click(object sender, EventArgs e)
        {
            fNumber = Convert.ToInt32(FirstNumberTextBox.Text);
            sNumber = Convert.ToInt32(SecondNumberTextBox.Text);

            loadLabel.Text = (fNumber -sNumber).ToString();
        }

        private void multiplicationButton_Click(object sender, EventArgs e)
        {
            fNumber = Convert.ToInt32(FirstNumberTextBox.Text);
            sNumber = Convert.ToInt32(SecondNumberTextBox.Text);

            loadLabel.Text = (fNumber*sNumber).ToString();

        }

        private void divisionButton_Click(object sender, EventArgs e)
        {
            fNumber = Convert.ToInt32(FirstNumberTextBox.Text);
            sNumber = Convert.ToInt32(SecondNumberTextBox.Text);

            loadLabel.Text = (fNumber /sNumber).ToString();
        }

        private void loadButton_Click(object sender, EventArgs e)
        {

        }
    }
}
