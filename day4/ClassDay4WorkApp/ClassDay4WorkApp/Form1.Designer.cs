﻿namespace ClassDay4WorkApp
{
    partial class RegistrationUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.lastNameLabel = new System.Windows.Forms.Label();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.maleRadioButton = new System.Windows.Forms.RadioButton();
            this.femaleRadioButton = new System.Windows.Forms.RadioButton();
            this.countrynameComboBox = new System.Windows.Forms.ComboBox();
            this.languagelabel = new System.Windows.Forms.Label();
            this.englishCheckBox = new System.Windows.Forms.CheckBox();
            this.spanishCheckBox = new System.Windows.Forms.CheckBox();
            this.showAllButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Location = new System.Drawing.Point(67, 43);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(57, 13);
            this.firstNameLabel.TabIndex = 0;
            this.firstNameLabel.Text = "First Name";
            // 
            // lastNameLabel
            // 
            this.lastNameLabel.AutoSize = true;
            this.lastNameLabel.Location = new System.Drawing.Point(67, 84);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Size = new System.Drawing.Size(58, 13);
            this.lastNameLabel.TabIndex = 1;
            this.lastNameLabel.Text = "Last Name";
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(153, 43);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(198, 20);
            this.firstNameTextBox.TabIndex = 2;
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(153, 84);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(198, 20);
            this.lastNameTextBox.TabIndex = 3;
            // 
            // maleRadioButton
            // 
            this.maleRadioButton.AutoSize = true;
            this.maleRadioButton.Location = new System.Drawing.Point(153, 136);
            this.maleRadioButton.Name = "maleRadioButton";
            this.maleRadioButton.Size = new System.Drawing.Size(48, 17);
            this.maleRadioButton.TabIndex = 4;
            this.maleRadioButton.TabStop = true;
            this.maleRadioButton.Text = "Male";
            this.maleRadioButton.UseVisualStyleBackColor = true;
            // 
            // femaleRadioButton
            // 
            this.femaleRadioButton.AutoSize = true;
            this.femaleRadioButton.Location = new System.Drawing.Point(270, 136);
            this.femaleRadioButton.Name = "femaleRadioButton";
            this.femaleRadioButton.Size = new System.Drawing.Size(59, 17);
            this.femaleRadioButton.TabIndex = 5;
            this.femaleRadioButton.TabStop = true;
            this.femaleRadioButton.Text = "Female";
            this.femaleRadioButton.UseVisualStyleBackColor = true;
            // 
            // countrynameComboBox
            // 
            this.countrynameComboBox.FormattingEnabled = true;
            this.countrynameComboBox.Items.AddRange(new object[] {
            "USA",
            "BD",
            "CANADA",
            "UK"});
            this.countrynameComboBox.Location = new System.Drawing.Point(153, 177);
            this.countrynameComboBox.Name = "countrynameComboBox";
            this.countrynameComboBox.Size = new System.Drawing.Size(198, 21);
            this.countrynameComboBox.TabIndex = 6;
            // 
            // languagelabel
            // 
            this.languagelabel.AutoSize = true;
            this.languagelabel.Location = new System.Drawing.Point(80, 253);
            this.languagelabel.Name = "languagelabel";
            this.languagelabel.Size = new System.Drawing.Size(55, 13);
            this.languagelabel.TabIndex = 7;
            this.languagelabel.Text = "Language";
            // 
            // englishCheckBox
            // 
            this.englishCheckBox.AutoSize = true;
            this.englishCheckBox.Location = new System.Drawing.Point(163, 253);
            this.englishCheckBox.Name = "englishCheckBox";
            this.englishCheckBox.Size = new System.Drawing.Size(60, 17);
            this.englishCheckBox.TabIndex = 8;
            this.englishCheckBox.Text = "English";
            this.englishCheckBox.UseVisualStyleBackColor = true;
            // 
            // spanishCheckBox
            // 
            this.spanishCheckBox.AutoSize = true;
            this.spanishCheckBox.Location = new System.Drawing.Point(270, 253);
            this.spanishCheckBox.Name = "spanishCheckBox";
            this.spanishCheckBox.Size = new System.Drawing.Size(64, 17);
            this.spanishCheckBox.TabIndex = 9;
            this.spanishCheckBox.Text = "Spanish";
            this.spanishCheckBox.UseVisualStyleBackColor = true;
            // 
            // showAllButton
            // 
            this.showAllButton.Location = new System.Drawing.Point(191, 305);
            this.showAllButton.Name = "showAllButton";
            this.showAllButton.Size = new System.Drawing.Size(75, 23);
            this.showAllButton.TabIndex = 10;
            this.showAllButton.Text = "Show All";
            this.showAllButton.UseVisualStyleBackColor = true;
            this.showAllButton.Click += new System.EventHandler(this.showAllButton_Click);
            // 
            // RegistrationUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 418);
            this.Controls.Add(this.showAllButton);
            this.Controls.Add(this.spanishCheckBox);
            this.Controls.Add(this.englishCheckBox);
            this.Controls.Add(this.languagelabel);
            this.Controls.Add(this.countrynameComboBox);
            this.Controls.Add(this.femaleRadioButton);
            this.Controls.Add(this.maleRadioButton);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(this.firstNameTextBox);
            this.Controls.Add(this.lastNameLabel);
            this.Controls.Add(this.firstNameLabel);
            this.Name = "RegistrationUI";
            this.Text = "Registration Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.Label lastNameLabel;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.RadioButton maleRadioButton;
        private System.Windows.Forms.RadioButton femaleRadioButton;
        private System.Windows.Forms.ComboBox countrynameComboBox;
        private System.Windows.Forms.Label languagelabel;
        private System.Windows.Forms.CheckBox englishCheckBox;
        private System.Windows.Forms.CheckBox spanishCheckBox;
        private System.Windows.Forms.Button showAllButton;
    }
}

