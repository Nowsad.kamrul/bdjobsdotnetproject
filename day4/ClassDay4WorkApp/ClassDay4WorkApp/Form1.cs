﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassDay4WorkApp
{
    public partial class RegistrationUI : Form
    {
        public string message;
        public RegistrationUI()
        {
            InitializeComponent();
        }

        private void showAllButton_Click(object sender, EventArgs e)
        {
            string firstName = firstNameTextBox.Text;
            string lastName = lastNameTextBox.Text;
            if (firstName !="" && lastName!="")
            {
              message= "Your full name is " + firstName + " "+lastName +" \n" ;
            }
            if (maleRadioButton.Checked)
            {
                message +="Gender is :Male"+"\n ";
            }
            if (femaleRadioButton.Checked)
            {
                message += "Gender is :Female"+ "\n ";

            }
            message += countrynameComboBox.Text +"\n";
            if(englishCheckBox.Checked)
            {
                message += "your language is english"+ "\n ";

            }
            if (spanishCheckBox.Checked)
            {
                message += "your lanuguage is spanish"+ " \n";
            }

            MessageBox.Show(message);
        }
    }
}
