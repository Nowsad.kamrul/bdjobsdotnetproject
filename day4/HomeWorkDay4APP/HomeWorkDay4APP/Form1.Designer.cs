﻿namespace HomeWorkDay4APP
{
    partial class CourseUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.codeTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.departmentGroupBox = new System.Windows.Forms.GroupBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.totalRecordLabel = new System.Windows.Forms.Label();
            this.departmentnameLabel = new System.Windows.Forms.Label();
            this.departmentCodeLabel = new System.Windows.Forms.Label();
            this.nameDataGridView = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // codeTextBox
            // 
            this.codeTextBox.Location = new System.Drawing.Point(201, 74);
            this.codeTextBox.Name = "codeTextBox";
            this.codeTextBox.Size = new System.Drawing.Size(222, 20);
            this.codeTextBox.TabIndex = 0;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(201, 129);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(222, 20);
            this.nameTextBox.TabIndex = 1;
            // 
            // departmentGroupBox
            // 
            this.departmentGroupBox.Controls.Add(this.searchButton);
            this.departmentGroupBox.Controls.Add(this.saveButton);
            this.departmentGroupBox.Controls.Add(this.totalRecordLabel);
            this.departmentGroupBox.Controls.Add(this.departmentnameLabel);
            this.departmentGroupBox.Controls.Add(this.departmentCodeLabel);
            this.departmentGroupBox.Controls.Add(this.codeTextBox);
            this.departmentGroupBox.Controls.Add(this.nameTextBox);
            this.departmentGroupBox.Location = new System.Drawing.Point(67, 41);
            this.departmentGroupBox.Name = "departmentGroupBox";
            this.departmentGroupBox.Size = new System.Drawing.Size(617, 260);
            this.departmentGroupBox.TabIndex = 2;
            this.departmentGroupBox.TabStop = false;
            this.departmentGroupBox.Text = "Department Record List";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(460, 70);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 6;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(348, 196);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 5;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // totalRecordLabel
            // 
            this.totalRecordLabel.AutoSize = true;
            this.totalRecordLabel.Location = new System.Drawing.Point(101, 201);
            this.totalRecordLabel.Name = "totalRecordLabel";
            this.totalRecordLabel.Size = new System.Drawing.Size(64, 13);
            this.totalRecordLabel.TabIndex = 4;
            this.totalRecordLabel.Text = "Total record";
            // 
            // departmentnameLabel
            // 
            this.departmentnameLabel.AutoSize = true;
            this.departmentnameLabel.Location = new System.Drawing.Point(86, 136);
            this.departmentnameLabel.Name = "departmentnameLabel";
            this.departmentnameLabel.Size = new System.Drawing.Size(93, 13);
            this.departmentnameLabel.TabIndex = 3;
            this.departmentnameLabel.Text = "Department Name";
            // 
            // departmentCodeLabel
            // 
            this.departmentCodeLabel.AutoSize = true;
            this.departmentCodeLabel.Location = new System.Drawing.Point(86, 74);
            this.departmentCodeLabel.Name = "departmentCodeLabel";
            this.departmentCodeLabel.Size = new System.Drawing.Size(90, 13);
            this.departmentCodeLabel.TabIndex = 2;
            this.departmentCodeLabel.Text = "Department Code";
            // 
            // nameDataGridView
            // 
            this.nameDataGridView.AllowUserToDeleteRows = false;
            this.nameDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.nameDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.code});
            this.nameDataGridView.Location = new System.Drawing.Point(95, 392);
            this.nameDataGridView.Name = "nameDataGridView";
            this.nameDataGridView.Size = new System.Drawing.Size(543, 119);
            this.nameDataGridView.TabIndex = 4;
            // 
            // name
            // 
            this.name.Frozen = true;
            this.name.HeaderText = "Name";
            this.name.MinimumWidth = 10;
            this.name.Name = "name";
            this.name.Width = 250;
            // 
            // code
            // 
            this.code.Frozen = true;
            this.code.HeaderText = "Code";
            this.code.MinimumWidth = 10;
            this.code.Name = "code";
            this.code.Width = 250;
            // 
            // CourseUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 594);
            this.Controls.Add(this.nameDataGridView);
            this.Controls.Add(this.departmentGroupBox);
            this.Name = "CourseUI";
            this.Text = "Department Course";
            this.departmentGroupBox.ResumeLayout(false);
            this.departmentGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox codeTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.GroupBox departmentGroupBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label totalRecordLabel;
        private System.Windows.Forms.Label departmentnameLabel;
        private System.Windows.Forms.Label departmentCodeLabel;
        private System.Windows.Forms.DataGridView nameDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn code;
    }
}

