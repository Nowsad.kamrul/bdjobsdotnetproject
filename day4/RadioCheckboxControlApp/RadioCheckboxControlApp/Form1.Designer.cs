﻿namespace RadioCheckboxControlApp
{
    partial class ControlUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioGroupBox = new System.Windows.Forms.GroupBox();
            this.oddRadioButton = new System.Windows.Forms.RadioButton();
            this.evenRadioButton = new System.Windows.Forms.RadioButton();
            this.showButton = new System.Windows.Forms.Button();
            this.cokeCheckBox = new System.Windows.Forms.CheckBox();
            this.pizzaCheckBox = new System.Windows.Forms.CheckBox();
            this.waterCheckBox = new System.Windows.Forms.CheckBox();
            this.orderButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.nameListView = new System.Windows.Forms.ListView();
            this.comboButton = new System.Windows.Forms.Button();
            this.nameComboBox = new System.Windows.Forms.ComboBox();
            this.radioGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioGroupBox
            // 
            this.radioGroupBox.Controls.Add(this.showButton);
            this.radioGroupBox.Controls.Add(this.evenRadioButton);
            this.radioGroupBox.Controls.Add(this.oddRadioButton);
            this.radioGroupBox.Location = new System.Drawing.Point(22, 23);
            this.radioGroupBox.Name = "radioGroupBox";
            this.radioGroupBox.Size = new System.Drawing.Size(257, 136);
            this.radioGroupBox.TabIndex = 0;
            this.radioGroupBox.TabStop = false;
            this.radioGroupBox.Text = "Radio Button Option";
            // 
            // oddRadioButton
            // 
            this.oddRadioButton.AutoSize = true;
            this.oddRadioButton.Location = new System.Drawing.Point(6, 31);
            this.oddRadioButton.Name = "oddRadioButton";
            this.oddRadioButton.Size = new System.Drawing.Size(49, 17);
            this.oddRadioButton.TabIndex = 0;
            this.oddRadioButton.TabStop = true;
            this.oddRadioButton.Text = "ODD";
            this.oddRadioButton.UseVisualStyleBackColor = true;
            // 
            // evenRadioButton
            // 
            this.evenRadioButton.AutoSize = true;
            this.evenRadioButton.Location = new System.Drawing.Point(133, 31);
            this.evenRadioButton.Name = "evenRadioButton";
            this.evenRadioButton.Size = new System.Drawing.Size(54, 17);
            this.evenRadioButton.TabIndex = 1;
            this.evenRadioButton.TabStop = true;
            this.evenRadioButton.Text = "EVEN";
            this.evenRadioButton.UseVisualStyleBackColor = true;
            // 
            // showButton
            // 
            this.showButton.Location = new System.Drawing.Point(74, 86);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(75, 23);
            this.showButton.TabIndex = 2;
            this.showButton.Text = "show";
            this.showButton.UseVisualStyleBackColor = true;
            this.showButton.Click += new System.EventHandler(this.showButton_Click);
            // 
            // cokeCheckBox
            // 
            this.cokeCheckBox.AutoSize = true;
            this.cokeCheckBox.Location = new System.Drawing.Point(62, 184);
            this.cokeCheckBox.Name = "cokeCheckBox";
            this.cokeCheckBox.Size = new System.Drawing.Size(51, 17);
            this.cokeCheckBox.TabIndex = 1;
            this.cokeCheckBox.Text = "Coke";
            this.cokeCheckBox.UseVisualStyleBackColor = true;
            // 
            // pizzaCheckBox
            // 
            this.pizzaCheckBox.AutoSize = true;
            this.pizzaCheckBox.Location = new System.Drawing.Point(62, 222);
            this.pizzaCheckBox.Name = "pizzaCheckBox";
            this.pizzaCheckBox.Size = new System.Drawing.Size(51, 17);
            this.pizzaCheckBox.TabIndex = 2;
            this.pizzaCheckBox.Text = "Pizza";
            this.pizzaCheckBox.UseVisualStyleBackColor = true;
            // 
            // waterCheckBox
            // 
            this.waterCheckBox.AutoSize = true;
            this.waterCheckBox.Location = new System.Drawing.Point(62, 257);
            this.waterCheckBox.Name = "waterCheckBox";
            this.waterCheckBox.Size = new System.Drawing.Size(55, 17);
            this.waterCheckBox.TabIndex = 3;
            this.waterCheckBox.Text = "Water";
            this.waterCheckBox.UseVisualStyleBackColor = true;
            // 
            // orderButton
            // 
            this.orderButton.Location = new System.Drawing.Point(78, 289);
            this.orderButton.Name = "orderButton";
            this.orderButton.Size = new System.Drawing.Size(75, 23);
            this.orderButton.TabIndex = 4;
            this.orderButton.Text = "Order";
            this.orderButton.UseVisualStyleBackColor = true;
            this.orderButton.Click += new System.EventHandler(this.orderButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(242, 184);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 5;
            // 
            // nameListView
            // 
            this.nameListView.Location = new System.Drawing.Point(360, 202);
            this.nameListView.Name = "nameListView";
            this.nameListView.Size = new System.Drawing.Size(121, 97);
            this.nameListView.TabIndex = 6;
            this.nameListView.UseCompatibleStateImageBehavior = false;
            // 
            // comboButton
            // 
            this.comboButton.Location = new System.Drawing.Point(383, 173);
            this.comboButton.Name = "comboButton";
            this.comboButton.Size = new System.Drawing.Size(75, 23);
            this.comboButton.TabIndex = 7;
            this.comboButton.Text = "ADD";
            this.comboButton.UseVisualStyleBackColor = true;
            this.comboButton.Click += new System.EventHandler(this.comboButton_Click);
            // 
            // nameComboBox
            // 
            this.nameComboBox.FormattingEnabled = true;
            this.nameComboBox.Items.AddRange(new object[] {
            "Nowsad",
            "Kamrul",
            "Ratan"});
            this.nameComboBox.Location = new System.Drawing.Point(360, 138);
            this.nameComboBox.Name = "nameComboBox";
            this.nameComboBox.Size = new System.Drawing.Size(121, 21);
            this.nameComboBox.TabIndex = 8;
            // 
            // ControlUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 359);
            this.Controls.Add(this.nameComboBox);
            this.Controls.Add(this.comboButton);
            this.Controls.Add(this.nameListView);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.orderButton);
            this.Controls.Add(this.waterCheckBox);
            this.Controls.Add(this.pizzaCheckBox);
            this.Controls.Add(this.cokeCheckBox);
            this.Controls.Add(this.radioGroupBox);
            this.Name = "ControlUI";
            this.Text = "ControlUI";
            this.radioGroupBox.ResumeLayout(false);
            this.radioGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox radioGroupBox;
        private System.Windows.Forms.Button showButton;
        private System.Windows.Forms.RadioButton evenRadioButton;
        private System.Windows.Forms.RadioButton oddRadioButton;
        private System.Windows.Forms.CheckBox cokeCheckBox;
        private System.Windows.Forms.CheckBox pizzaCheckBox;
        private System.Windows.Forms.CheckBox waterCheckBox;
        private System.Windows.Forms.Button orderButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ListView nameListView;
        private System.Windows.Forms.Button comboButton;
        private System.Windows.Forms.ComboBox nameComboBox;
    }
}

