﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadioCheckboxControlApp
{
    public partial class ControlUI : Form
    {
        public ControlUI()
        {
            InitializeComponent();
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            if (oddRadioButton.Checked)
                MessageBox.Show("Odd Button Selected");
            if (evenRadioButton.Checked)
            {
                MessageBox.Show("Even Button Selected");
            }
        }

        private void orderButton_Click(object sender, EventArgs e)
        {
            if (cokeCheckBox.Checked && pizzaCheckBox.Checked && waterCheckBox.Checked)
            {
                MessageBox.Show("Give me pizza coke and water");
            }
            else
            {
                if (cokeCheckBox.Checked)
                {
                    MessageBox.Show("Give me coke");
                }
                if (pizzaCheckBox.Checked)
                {
                    MessageBox.Show("Give me pizza");
                }
                if (waterCheckBox.Checked)
                {
                    MessageBox.Show("Give me water");
                }
            }
        }

        private void comboButton_Click(object sender, EventArgs e)
        {
            nameListView.Items.Add(nameComboBox.Text);
        }
    }
}
