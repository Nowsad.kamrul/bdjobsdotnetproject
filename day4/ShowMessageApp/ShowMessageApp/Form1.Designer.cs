﻿namespace ShowMessageApp
{
    partial class FirstAppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showButton = new System.Windows.Forms.Button();
            this.firstAppTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // showButton
            // 
            this.showButton.Location = new System.Drawing.Point(114, 119);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(75, 23);
            this.showButton.TabIndex = 0;
            this.showButton.Text = "show";
            this.showButton.UseVisualStyleBackColor = true;
            this.showButton.Click += new System.EventHandler(this.showButton_Click);
            // 
            // firstAppTextBox
            // 
            this.firstAppTextBox.Location = new System.Drawing.Point(104, 80);
            this.firstAppTextBox.Name = "firstAppTextBox";
            this.firstAppTextBox.Size = new System.Drawing.Size(100, 20);
            this.firstAppTextBox.TabIndex = 1;
            this.firstAppTextBox.TextChanged += new System.EventHandler(this.firstAppTextBox_TextChanged);
            // 
            // FirstAppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.firstAppTextBox);
            this.Controls.Add(this.showButton);
            this.Name = "FirstAppForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FirstAppForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button showButton;
        private System.Windows.Forms.TextBox firstAppTextBox;
    }
}

