﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorApp
{
    public partial class CalculatorAppUI : Form
    {
        public CalculatorAppUI()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            int result = Convert.ToInt32(firstNumbertextBox.Text) + Convert.ToInt32(secondNumbertextBox.Text);
            MessageBox.Show(result.ToString());
        }

        private void subtractionButton_Click(object sender, EventArgs e)
        {
            int result = Convert.ToInt32(firstNumbertextBox.Text) -Convert.ToInt32(secondNumbertextBox.Text);
            MessageBox.Show(result.ToString());
        }

        private void multiplicationButton_Click(object sender, EventArgs e)
        {
            int result = Convert.ToInt32(firstNumbertextBox.Text) * Convert.ToInt32(secondNumbertextBox.Text);
            MessageBox.Show(result.ToString());
        }

        private void divisionButton_Click(object sender, EventArgs e)
        {
            int result = Convert.ToInt32(firstNumbertextBox.Text) / Convert.ToInt32(secondNumbertextBox.Text);
            MessageBox.Show(result.ToString());
        }
    }
}
