﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonalInformationApp
{
    public partial class PersonalInformationUI : Form
    {

        string firstName;
        string lastName;
        string fathersName;
        string mothersName;
        string address;
     
         


        public PersonalInformationUI()
        {
            InitializeComponent();
        }

        private void PersonalInformationUI_Load(object sender, EventArgs e)
        {
          
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            firstName = firstNameTextBox.Text;
            lastName = lastNameTextBox.Text;
            fathersName = fathersNameTextBox.Text;
            mothersName = mothersNameTextBox.Text;
            address = addressTextBox.Text;

            firstNameTextBox.Text = "";
            lastNameTextBox.Text = "";
            fathersNameTextBox.Text = "";
            mothersNameTextBox.Text = "";
            addressTextBox.Text = "";

        }

        private void showAllInformatioButton_Click(object sender, EventArgs e)



        {
            firstNameTextBox.Text = firstName;
            lastNameTextBox.Text = lastName;
            fathersNameTextBox.Text = fathersName;
            mothersNameTextBox.Text = mothersName;
            addressTextBox.Text = address;



            MessageBox.Show("My name is " + firstName+ lastName + '\n' + "My Parents Name is " + fathersName+ '\n' + mothersName + '\n' +
                "My address is " + address);
        }

        private void nameButton_Click(object sender, EventArgs e)
        {
            firstNameTextBox.Text = firstName;
            lastNameTextBox.Text = lastName;
            MessageBox.Show("My  name is" + firstName + '\n' + lastName);

        }

        private void parentsNameButton_Click(object sender, EventArgs e)
        {
            fathersNameTextBox.Text = fathersName;
            mothersNameTextBox.Text = mothersName;
            MessageBox.Show("My parent name is" + fathersName + '\n' + mothersName);
        }

        private void addressButton_Click(object sender, EventArgs e)
        {
            addressTextBox.Text = address;
            MessageBox.Show("My address is " + address);
        }
    }
}
