﻿namespace PlanetApp
{
    partial class PlanetUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.planetNameComboBox = new System.Windows.Forms.ComboBox();
            this.showPlanetButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // planetNameComboBox
            // 
            this.planetNameComboBox.FormattingEnabled = true;
            this.planetNameComboBox.Items.AddRange(new object[] {
            "Earth",
            "Pluto",
            "Neptune",
            "Venus",
            "Mars",
            "Jupiter",
            "Saturn",
            "Uranus"});
            this.planetNameComboBox.Location = new System.Drawing.Point(133, 86);
            this.planetNameComboBox.Name = "planetNameComboBox";
            this.planetNameComboBox.Size = new System.Drawing.Size(155, 21);
            this.planetNameComboBox.TabIndex = 0;
            // 
            // showPlanetButton
            // 
            this.showPlanetButton.Location = new System.Drawing.Point(270, 128);
            this.showPlanetButton.Name = "showPlanetButton";
            this.showPlanetButton.Size = new System.Drawing.Size(75, 23);
            this.showPlanetButton.TabIndex = 1;
            this.showPlanetButton.Text = "Show Planet";
            this.showPlanetButton.UseVisualStyleBackColor = true;
            this.showPlanetButton.Click += new System.EventHandler(this.showPlanetButton_Click);
            // 
            // PlanetUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 359);
            this.Controls.Add(this.showPlanetButton);
            this.Controls.Add(this.planetNameComboBox);
            this.Name = "PlanetUI";
            this.Text = "Planet UI";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox planetNameComboBox;
        private System.Windows.Forms.Button showPlanetButton;
    }
}

